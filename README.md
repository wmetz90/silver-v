# Silver-V Emulator

Silver-V is a CPU emulator implementing the RISC-V RV32GC (G = IMAFD, Zicsr,
Zifencei) ISA. This is simply made by one person for fun and as a way to learn,
do not expect the best performance or code. Silver-V is currently based on the
2019-12-13 draft of the RISC-V specification.