#ifndef __silver_v_m_ops_h
#define __silver_v_m_ops_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"
#include "operands.h"



/*
 * =============================================================================
 *   m-ops.h
 *
 *   Prototypes functions to handle operations belonging to the M (integer
 *   multiply/divide) extension to the RV32I instruction set.
 * =============================================================================
 */



void sv_mul(operand_r_t o);
void sv_mulh(operand_r_t o);
void sv_mulhsu(operand_r_t o);
void sv_mulh(operand_r_t o);
void sv_div(operand_r_t o);
void sv_divu(operand_r_t o);
void sv_rem(operand_r_t o);
void sv_remu(operand_r_t o);



#endif