#ifndef __silver_v_c_ops_h
#define __silver_v_c_ops_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "cpu.h"
#include "types.h"
#include "mem.h"
#include "operands.h"
#include "registers.h"



/*
 * =============================================================================
 *   c-ops.h
 *
 *   Prototypes functions to handle operations belonging to the C extension
 *   (compressed operations) to the RV32I instruction set.
 * =============================================================================
 */

// SP-Relative Loads

void sv_c_lwsp(operand_ci_t o);
void sv_c_flwsp(operand_ci_t o);
void sv_c_fldsp(operand_ci_t o);

// SP-Relative Stores

void sv_c_swsp(operand_css_t o);
void sv_c_fswsp(operand_css_t o);
void sv_c_fsdsp(operand_css_t o);

// Loads

void sv_c_lw(operand_cl_t o);
void sv_c_flw(operand_cl_t o);
void sv_c_fld(operand_cl_t o);

// Stores

void sv_c_sw(operand_cs_t o);
void sv_c_fsw(operand_cs_t o);
void sv_c_fsd(operand_cs_t o);

// Unconditional Jumps
void sv_c_j(operand_cj_t o);
void sv_c_jal(operand_cj_t o);
void sv_c_jr(operand_cr_t o);
void sv_c_jalr(operand_cr_t o);

// Branches

void sv_c_beqz(operand_cb_t o);
void sv_c_bnez(operand_cb_t o);

// Constant Generation

void sv_c_li(operand_ci_t o);
void sv_c_lui(operand_ci_t o);

// Integer Register-Immediate

void sv_c_addi(operand_ci_t o);
void sv_c_addi16sp(operand_ci_t o);
void sv_c_addi4spn(operand_ciw_t o);
void sv_c_slli(operand_ci_t o);
void sv_c_srli(operand_cb_t o);
void sv_c_srai(operand_cb_t o);
void sv_c_andi(operand_cb_t o);

// Integer Register-Register

void sv_c_mv(operand_cr_t o);
void sv_c_add(operand_cr_t o);
void sv_c_xor(operand_ca_t o);
void sv_c_or(operand_ca_t o);
void sv_c_and(operand_ca_t o);
void sv_c_sub(operand_ca_t o);

// Misc

void sv_c_nop();
void sv_c_ebreak();
void sv_illegal();




#endif