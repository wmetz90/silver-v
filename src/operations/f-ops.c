#include "f-ops.h"

/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   f-ops.c
 *
 *   Implements functions to handle operations belonging to the F extension
 *   (single-precision floating point) to the RV32I instruction set.
 * =============================================================================
 */



void sv_flw(operand_i_t o);

void sv_fsw(operand_s_t o);

void sv_fmadd_s(operand_r4_t o);

void sv_fmsub_s(operand_r4_t o);

void sv_fnmsub_s(operand_r4_t o);

void sv_fnmadd_s(operand_r4_t o);

void sv_fadd_s(operand_r_t o);

void sv_fsub_s(operand_r_t o);

void sv_fmul_s(operand_r_t o);

void sv_fdiv_s(operand_r_t o);

void sv_fsqrt_s(operand_r_t o);

void sv_fsgnj_s(operand_r_t o);

void sv_fsgnjn_s(operand_r_t o);

void sv_fsgnjx_s(operand_r_t o);

void sv_fmin_s(operand_r_t o);

void sv_fmax_s(operand_r_t o);

void sv_fcvt_w_s(operand_r_t o);

void sv_fcvt_wu_s(operand_r_t o);

void sv_fcvt_s_w(operand_r_t o);

void sv_fcvt_s_wu(operand_r_t o);

void sv_fmv_x_w(operand_r_t o);

void sv_fmv_w_x(operand_r_t o);

void sv_feq_s(operand_r_t o);

void sv_flt_s(operand_r_t o);

void sv_fle_s(operand_r_t o);

void sv_fclass_s(operand_r_t o);