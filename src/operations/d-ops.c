#include "d-ops.h"

/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   d-ops.c
 *
 *   Implements functions to handle operations belonging to the D extension
 *   (double-precision floating point) to the RV32I instruction set.
 * =============================================================================
 */



void sv_fld(operand_i_t o);

void sv_fsd(operand_s_t o);

void sv_fmadd_d(operand_r4_t o);

void sv_fmsub_d(operand_r4_t o);

void sv_fnmsub_d(operand_r4_t o);

void sv_fnmadd_d(operand_r4_t o);

void sv_fadd_d(operand_r_t o);

void sv_fsub_d(operand_r_t o);

void sv_fmul_d(operand_r_t o);

void sv_fdiv_d(operand_r_t o);

void sv_fsqrt_d(operand_r_t o);

void sv_fsgnj_d(operand_r_t o);

void sv_fsgnjn_d(operand_r_t o);

void sv_fsgnjx_d(operand_r_t o);

void sv_fmin_d(operand_r_t o);

void sv_fmax_d(operand_r_t o);

void sv_feq_d(operand_r_t o);

void sv_flt_d(operand_r_t o);

void sv_fle_d(operand_r_t o);

void sv_fclass_d(operand_r_t o);

void sv_fcvt_s_d(operand_r_t o);

void sv_fcvt_d_s(operand_r_t o);

void sv_fcvt_w_d(operand_r_t o);

void sv_fcvt_wu_d(operand_r_t o);

void sv_fcvt_d_w(operand_r_t o);

void sv_fcvt_d_wu(operand_r_t o);