#include "i-ops.h"

/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "cpu.h"
#include "mem.h"
#include "operands.h"



/*
 * =============================================================================
 *   i-ops.c
 *
 *   Implements the operations defined in the RV32I instruction set.
 * =============================================================================
 */


// LUI - Load Upper Immediate
//
// Takes a 20 bit immediate value and loads it into the upper 20 bits of the
// destination register. The lower 12 bits are set to zero. Used to build 32-bit
// constants.
void sv_lui(operand_u_t o) {
  cpu.reg[o.rd] = o.imm << 12;
}

// AUIPC - Add Upper Immediate to Program Counter
//
// Takes a 20 bit immediate value and adds it to the upper 20 bits of the
// program counter. Used to build PC-relative addresses.
void sv_lui(operand_u_t o) {
  cpu.reg[o.rd] = (o.imm << 12) + cpu.pc;
}

void sv_jal(operand_j_t o) {
  if (o.rd) {
    cpu.reg[o.rd] = cpu.pc + 4;
  }

  cpu.pc += ((signed)o.imm << 1);
}

void sv_jalr(operand_i_t o) {
  if (o.rd) {
    cpu.reg[o.rd] = cpu.pc + 4;
  }

  cpu.pc = (cpu.reg[o.rs1] + (signed)o.imm) & 0xFFFFFFFE;
}

// BEQ - Branch if EQual
//
// Jumps to a given offset if the values in the two specified registers are
// equal.
void sv_beq(operand_b_t o) {
  if (cpu.reg[o.rs1] == cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// BNE - Branch if Not Equal
//
// Jumps to a given offset if the values in the two specified registers are not
// equal.
void sv_bne(operand_b_t o) {
  if (cpu.reg[o.rs1] != cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// BLT - Branch if Less Than
//
// Jumps to a given offset if the value in rs1 is less than the value in rs2.
void sv_blt(operand_b_t o) {
  if ((signed)cpu.reg[o.rs1] < (signed)cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// BGE - Branch if Greater than or Equal
//
// Jumps to a given offset if the value in rs1 is greater than or equal to the
// value in rs2.
void sv_bge(operand_b_t o) {
  if ((signed)cpu.reg[o.rs1] >= (signed)cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// BLTU - Branch if Less Than (Unsigned)
//
// The same as BLT, but treats the values as unsigned integers rather than
// signed ones.
void sv_bltu(operand_b_t o) {
  if (cpu.reg[o.rs1] < cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// BGEU - Branch if Greater than or Equal (Unsigned)
//
// The same as BGE, but treats the values as unsigned integers rather than
// signed ones.
void sv_bgeu(operand_b_t o) {
  if (cpu.reg[o.rs1] >= cpu.reg[o.rs2]) {
    cpu.pc += ((signed)o.imm) << 1;
  }
}

// LB - Load Byte
//
// Loads an 8-bit byte from memory, using the value from the given register rs1
// added to a 12-bit immediate signed offset as the address to load from, and
// sign-extends it to fit the 32 bit register.
void sv_lb(operand_i_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  cpu.reg[o.rd] = (signed)ram_read(addr);
}

// LH - Load Half word
//
// Loads a 16-bit half word from memory (in little endian form, though this is
// not standard), using the value from the given register rs1 added to a 12-bit
// immediate signed offset as the address to load from, and sign-extends it to
// fit the 32 bit register.
void sv_lh(operand_i_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize + 1) return;

  u16 value = ram_read(addr);
  value     = ram_read(addr+1) << 8;

  cpu.reg[o.rd] = (signed)value;
}

// LW - Load Word
//
// Loads a 32-bit word from memory (in little endian form, though this is not
// standard), using the value from the given register rs1 added to a 12-bit
// immediate signed offset as the address to load from.
void sv_lw(operand_i_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize + 3) return;

  cpu.reg[o.rd] = ram_read(addr);
  cpu.reg[o.rd] = ram_read(addr+1) << 8;
  cpu.reg[o.rd] = ram_read(addr+2) << 16;
  cpu.reg[o.rd] = ram_read(addr+3) << 24;
}

// LBU - Load Byte Unsigned
//
// The same as LB, but zero-extends the value instead of sign extending.
void sv_lbu(operand_i_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  cpu.reg[o.rd] = ram_read(addr);
}

// LHU - Load Half Unsigned
//
// The same as LH, but zero-extends the value instead of sign extending.
void sv_lhu(operand_i_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize + 1) return;

  cpu.reg[o.rd] = ram_read(addr);
  cpu.reg[o.rd] = ram_read(addr+1) << 8;
}

// SB - Store Byte
//
// Stores an 8-bit byte into memory, using the value from the given register rs1
// added to a 12-bit immediate signed offset as the address to store to. The
// value stored is the lower 8 bits of the given register rs2.
void sv_sb(operand_s_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize) return;

  u8 data = cpu.reg[o.rs2] & 0xFF;

  ram_write(addr, data);
}

// SH - Store Half word
//
// Stores a 16-bit half word into memory (in little endian form, though this is
// not standard), using the value from the given register rs1 added to a 12-bit
// immediate signed offset as the address to store to. The value stored is the
// lower 16 bits of the given register rs2.
void sv_sh(operand_s_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize + 1) return;

  u16 data = cpu.reg[o.rs2] & 0xFFFF;

  ram_write(addr, data & 0xFF);
  ram_write(addr+1, (data & 0xFF00) >> 8);
}

// SW - Store Word
//
// Stores a 32-bit word into memory (in little endian form, though this is not
// standard), using the value from the given register rs1 added to a 12-bit
// immediate signed offset as the address to store to. The value stored is the
// value in the given register rs2.
void sv_sw(operand_s_t o) {
  u32 addr = cpu.reg[o.rs1] + (signed)o.imm;

  if (addr >= ramsize + 3) return;

  u32 data = cpu.reg[o.rs2];

  ram_write(addr, data & 0xFF);
  ram_write(addr+1, (data & 0xFF00) >> 8);
  ram_write(addr+2, (data & 0xFF0000) >> 16);
  ram_write(addr+3, (data & 0xFF000000) >> 24);
}

// ADDI - ADD Immediate
//
// Takes a 12-bit immediate value, sign extends it, and adds it to the value in
// the given register rs1. The result is stored in the given register rd.
// `ADDI rd, rs1, 0` essentially moves the value in rs1 to rd, and is the
// implementation of the assembler pseudoinstruction `MV rd, rs1`.
void sv_addi(operand_i_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] + (signed)o.imm;
}

// NOP - No OPeration
//
// A pseudoinstruction that is simply used to tick the program counter without
// performing any operation. This is not an actual instruction, but it is called
// if an instruction would have no effect in order to save computation power.
void sv_nop() {
  return;
}

// SLTI - Set if Less Than Immediate
//
// Places the value 1 into register rd if the value in register rs1 is less
// than the given 12-bit sign-extended immediate.
void sv_slti(operand_i_t o) {
  if ((signed)cpu.reg[o.rs1] < (signed)o.imm)
    cpu.reg[o.rd] = 1;
  else
    cpu.reg[o.rd] = 0;
}

// SLTIU - Set if Less Than Immediate Unsigned
//
// The same as SLTI, except the immediate is zero-extended instead of
// sign-extended. `SLTIU rd, rs1, 1` sets rd to 1 if rs1 equals zero, and sets
// rd to zero in every other case, which is the implementation for the
// assembler pseudoinstruction `SEQZ rd, rs`.
void sv_sltiu(operand_i_t o) {
  if (cpu.reg[o.rs1] < (unsigned)((int)o.imm))
    cpu.reg[o.rd] = 1;
  else
    cpu.reg[o.rd] = 0;
}

// XORI - eXclusive OR Immediate
//
// Performs a bitwise XOR operation on the value in rs1 and the 12-bit
// sign-extended immediate value. The result is placed into register rd.
// `XORI rd, rs1, -1` performs what is essentially a bitwise NOT instruction,
// and is the implementation for the assembler pseudoinstruction `NOT rd, rs`.
void sv_xori(operand_i_t o) {
  // If the immediate is -1, act as a NOT instruction.
  if ((signed)o.imm == -1)
    cpu.reg[o.rd] = ~cpu.reg[o.rs1];
  else
    cpu.reg[o.rd] = cpu.reg[o.rs1] ^ (signed)o.imm;
}

// ORI - OR Immediate
//
// Performs a bitwise OR operation on the value in rs1 and the 12-bit
// sign-extended immediate value. The result is placed into register rd.
void sv_ori(operand_i_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] | (signed)o.imm;
}

// ANDI - AND Immediate
//
// Performs a bitwise AND operation on the value in rs1 and the 12-bit
// sign-extended immediate value. The result is placed into register rd.
void sv_andi(operand_i_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] & (signed)o.imm;
}

// SLLI - Shift Left Logical Immediate
//
// Performs a logical left shift on the value in rs1. The number of bits to
// shift is determined by the lower 5 bits of the 12-bit immediate value. The
// result is placed in register rd.
void sv_slli(operand_i_t o) {
  int shift = o.imm & 0x1F;

  cpu.reg[o.rd] = cpu.reg[o.rs1] << shift;
}

// SLRI - Shift Right Logical Immediate
//
// Performs a logical right shift on the value in rs1. The number of bits to
// shift is determined by the lower 5 bits of the 12-bit immediate value. The
// result is placed in register rd.
void sv_srli(operand_i_t o) {
  int shift = o.imm & 0x1F;

  int mask = ~(-1 << shift) << (32 - shift);
  cpu.reg[o.rd] = ~mask & ((cpu.reg[o.rs1] >> shift) | mask);
}

// Shift Right Arithmetic Immediate
//
// Performs an arithmetic right shift on the value in rs1. The number of bits
// to shift is determined by the lower 5 bits of the 12-bit immediate value.
// The result is placed in register rd. The difference between arithmetic and
// logical right shifts is that the sign is kept during arithmetic right shifts,
// meaning the left is filled with ones when a negative value is shifted.
// Logical shifts simply fill the left with zeroes.
void sv_srai(operand_i_t o) {
  int shift = o.imm & 0x1F;

  u32 ux = (unsigned)cpu.reg[o.rs1];
  u32 signbit = ux >> 31;
  cpu.reg[o.rd] = (ux >> shift) | (((0 - signbit) << 1) << (31 - shift));
}

// ADD
//
// Adds the values in registers rs1 and rs2, and stores the result into
// register rd.
void sv_add(operand_r_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] + cpu.reg[o.rs2];
}

// SUB - SUBtract
//
// Subtracts the value in register rs2 from the value in register rs1. The
// result is stored into register rd.
void sv_sub(operand_r_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] - cpu.reg[o.rs2];
}

// SLT - Set if Less Than
//
// Places 1 into register rd if the value in register rs1 is less than the
// value in register rs2. These values are treated as signed integers.
void sv_slt(operand_r_t o) {
  if ((signed)cpu.reg[o.rs1] < (signed)o.rs2)
    cpu.reg[o.rd] = 1;
  else
    cpu.reg[o.rd] = 0;
}

// SLTU - Set if Less Than Unsigned
//
// Places 1 into register rd if the value in register rs1 is less than the
// value in register rs2. These values are treated as unsigned integers.
void sv_sltu(operand_r_t o) {
  if (cpu.reg[o.rs1] < (unsigned)o.rs2)
    cpu.reg[o.rd] = 1;
  else
    cpu.reg[o.rd] = 0;
}

// XOR - eXclusive OR
//
// Performs a bitwise XOR operation on the values in registers rs1 and rs2. The
// result is placed into register rd.
void sv_xor(operand_r_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] ^ cpu.reg[o.rs2];
}

// OR
//
// Performs a bitwise OR operation on the values in registers rs1 and rs2. The
// result is placed into register rd.
void sv_or(operand_r_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] | cpu.reg[o.rs2];
}

// AND
//
// Performs a bitwise AND operation on the values in registers rs1 and rs2. The
// result is placed into register rd.
void sv_and(operand_r_t o) {
  cpu.reg[o.rd] = cpu.reg[o.rs1] & cpu.reg[o.rs2];
}

// SLL - Shift Left Logical
//
// Shifts the value in register rs1 left by the number of bits specified in the
// lower 5 bits of register rs2. The result is placed into register rd.
void sv_sll(operand_r_t o) {
  int shift = cpu.reg[o.rs2] & 0x1F;

  cpu.reg[o.rd] = cpu.reg[o.rs1] << shift;
}

// SRL - Shift Right Logical
//
// Performs a logical right right shift on the value in register rs1. The
// number of bits to shift is determined by the lower 5 bits of register rs2.
// The result is placed into register rd.
void sv_srl(operand_r_t o) {
  int shift = cpu.reg[o.rs2] & 0x1F;

  int mask = ~(-1 << shift) << (32 - shift);
  cpu.reg[o.rd] = ~mask & ((cpu.reg[o.rs1] >> shift) | mask);
}

// SRA - Shift Right Arithmetic
//
// Performs an arithmetic right right shift on the value in register rs1. The
// number of bits to shift is determined by the lower 5 bits of register rs2.
// The result is placed into register rd. The difference between arithmetic and
// logical right shifts is that the sign is kept during arithmetic right shifts,
// meaning the left is filled with ones when a negative value is shifted.
// Logical shifts simply fill the left with zeroes.
void sv_sra(operand_r_t o) {
  int shift = cpu.reg[o.rs2] & 0x1F;

  u32 ux = (unsigned)cpu.reg[o.rs1];
  u32 signbit = ux >> 31;
  cpu.reg[o.rd] = (ux >> shift) | (((0 - signbit) << 1) << (31 - shift));
}

typedef struct {
  unsigned fm : 4;
  unsigned pi : 1;
  unsigned po : 1;
  unsigned pr : 1;
  unsigned pw : 1;
  unsigned si : 1;
  unsigned so : 1;
  unsigned sr : 1;
  unsigned sw : 1;
} fence_fields_t;

void sv_fence(operand_i_t o) {
  fence_fields_t ff;
  ff.fm = (o.imm & 0xF00) >> 8;
  ff.pi = (o.imm & 0x80)  >> 7;
  ff.po = (o.imm & 0x40)  >> 6;
  ff.pr = (o.imm & 0x20)  >> 5;
  ff.pw = (o.imm & 0x10)  >> 4;
  ff.si = (o.imm & 0x08)  >> 3;
  ff.so = (o.imm & 0x04)  >> 2;
  ff.sr = (o.imm & 0x02)  >> 1;
  ff.sw = (o.imm & 0x01);
}

void sv_ecall();

void sv_ebreak();