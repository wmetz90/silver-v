#include "c-ops.h"

/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   c-ops.c
 *
 *   Implements functions to handle operations belonging to the C extension
 *   (compressed operations) to the RV32I instruction set.
 * =============================================================================
 */



void sv_c_lwsp(operand_ci_t o) {
  if (!o.rs1) return;

  u32 imm =  (o.imm & 0b000011) << 6;
  imm     |= (o.imm & 0b011100);
  imm     |= (o.imm & 0b100000);
  imm     += x2;

  cpu.reg[o.rs1] =  ram_read(imm);
  cpu.reg[o.rs1] |= ram_read(imm + 1);
  cpu.reg[o.rs1] |= ram_read(imm + 2);
  cpu.reg[o.rs1] |= ram_read(imm + 3);
}

void sv_c_flwsp(operand_ci_t o) {
  if (!o.rs1) return;

  u32 imm =  (o.imm & 0b001111) << 6;
  imm     |= (o.imm & 0b010000);
  imm     |= (o.imm & 0b100000);
  imm     += x2;

  cpu.f_reg[o.rs1] =  ram_read(imm);
  cpu.f_reg[o.rs1] |= ram_read(imm + 1) << 8;
  cpu.f_reg[o.rs1] |= ram_read(imm + 2) << 16;
  cpu.f_reg[o.rs1] |= ram_read(imm + 3) << 24;
}

void sv_c_fldsp(operand_ci_t o) {
  if (!o.rs1) return;

  u32 imm =  (o.imm & 0b000011) << 6;
  imm     |= (o.imm & 0b011100);
  imm     |= (o.imm & 0b100000);
  imm     += x2;

  cpu.f_reg[o.rs1] =  ram_read(imm);
  cpu.f_reg[o.rs1] |= ram_read(imm + 1) << 8;
  cpu.f_reg[o.rs1] |= ram_read(imm + 2) << 16;
  cpu.f_reg[o.rs1] |= ram_read(imm + 3) << 24;
  cpu.f_reg[o.rs1] |= ram_read(imm + 4) << 32;
  cpu.f_reg[o.rs1] |= ram_read(imm + 5) << 40;
  cpu.f_reg[o.rs1] |= ram_read(imm + 6) << 48;
  cpu.f_reg[o.rs1] |= ram_read(imm + 7) << 56;
}

void sv_c_swsp(operand_css_t o) {
  u32 imm =  (o.imm & 0b000011) << 6;
  imm     |= (o.imm & 0b111100);
  imm     += x2;

  ram_write(imm,      cpu.reg[o.rs2]        & 0xFF);
  ram_write(imm + 1, (cpu.reg[o.rs2] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.reg[o.rs2] >> 16) & 0xFF);
  ram_write(imm + 3, (cpu.reg[o.rs2] >> 24) & 0xFF);
}

void sv_c_fswsp(operand_css_t o) {
  u32 imm =  (o.imm & 0b000011) << 6;
  imm     |= (o.imm & 0b111100);
  imm     += x2;

  ram_write(imm,      cpu.f_reg[o.rs2]        & 0xFF);
  ram_write(imm + 1, (cpu.f_reg[o.rs2] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.f_reg[o.rs2] >> 16) & 0xFF);
  ram_write(imm + 3, (cpu.f_reg[o.rs2] >> 24) & 0xFF);
}

void sv_c_fsdsp(operand_css_t o) {
  u32 imm =  (o.imm & 0b000111) << 6;
  imm     |= (o.imm & 0b111000);
  imm     += x2;

  ram_write(imm,      cpu.f_reg[o.rs2]        & 0xFF);
  ram_write(imm + 1, (cpu.f_reg[o.rs2] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.f_reg[o.rs2] >> 16) & 0xFF);
  ram_write(imm + 3, (cpu.f_reg[o.rs2] >> 24) & 0xFF);
  ram_write(imm + 4, (cpu.f_reg[o.rs2] >> 32) & 0xFF);
  ram_write(imm + 5, (cpu.f_reg[o.rs2] >> 40) & 0xFF);
  ram_write(imm + 6, (cpu.f_reg[o.rs2] >> 48) & 0xFF);
  ram_write(imm + 7, (cpu.f_reg[o.rs2] >> 56) & 0xFF);
}

void sv_c_lw(operand_cl_t o) {
  u32 imm =  (o.imm & 0b00001) << 6;
  imm     |= (o.imm & 0b11110) << 1;
  imm     += cpu.reg[o.rs1];

  cpu.reg[o.rs2 + 8] =  ram_read(imm);
  cpu.reg[o.rs2 + 8] |= ram_read(imm + 1) << 8;
  cpu.reg[o.rs2 + 8] |= ram_read(imm + 2) << 16;
  cpu.reg[o.rs2 + 8] |= ram_read(imm + 3) << 24;
}

void sv_c_flw(operand_cl_t o) {
  u64 imm =  (o.imm & 0b00001) << 6;
  imm     |= (o.imm & 0b11110) << 1;
  imm     += cpu.reg[o.rs1];

  cpu.f_reg[o.rs2 + 8] =  ram_read(imm);
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 1) << 8;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 2) << 16;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 3) << 24;
}

void sv_c_fld(operand_cl_t o) {
  u32 imm =  (o.imm &0b00011) << 6;
  imm     |= (o.imm &0b11100) << 1;
  imm     += cpu.reg[o.rs1];

  cpu.f_reg[o.rs2 + 8] =  ram_read(imm);
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 1) << 8;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 2) << 16;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 3) << 24;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 4) << 32;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 5) << 40;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 6) << 48;
  cpu.f_reg[o.rs2 + 8] |= ram_read(imm + 7) << 56;
}

void sv_c_sw(operand_cs_t o) {
  u32 imm =  (o.imm &0b00001) << 6;
  imm     |= (o.imm &0b11110) << 1;
  imm     += cpu.reg[o.rs1];

  ram_write(imm,      cpu.reg[o.rs2 + 8]        & 0xFF);
  ram_write(imm + 1, (cpu.reg[o.rs2 + 8] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.reg[o.rs2 + 8] >> 16) & 0xFF);
  ram_write(imm + 1, (cpu.reg[o.rs2 + 8] >> 24) & 0xFF);
}

void sv_c_fsw(operand_cs_t o) {
  u32 imm =  (o.imm &0b00001) << 6;
  imm     |= (o.imm &0b11110) << 1;
  imm     += cpu.reg[o.rs1];

  ram_write(imm,      cpu.f_reg[o.rs2 + 8]        & 0xFF);
  ram_write(imm + 1, (cpu.f_reg[o.rs2 + 8] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.f_reg[o.rs2 + 8] >> 16) & 0xFF);
  ram_write(imm + 1, (cpu.f_reg[o.rs2 + 8] >> 24) & 0xFF);
}

void sv_c_fsd(operand_cs_t o) {
  u32 imm =  (o.imm &0b00011) << 6;
  imm     |= (o.imm &0b11100) << 1;
  imm     += cpu.reg[o.rs1];

  ram_write(imm,      cpu.f_reg[o.rs2 + 8]        & 0xFF);
  ram_write(imm + 1, (cpu.f_reg[o.rs2 + 8] >> 8)  & 0xFF);
  ram_write(imm + 2, (cpu.f_reg[o.rs2 + 8] >> 16) & 0xFF);
  ram_write(imm + 3, (cpu.f_reg[o.rs2 + 8] >> 24) & 0xFF);
  ram_write(imm + 4, (cpu.f_reg[o.rs2 + 8] >> 32) & 0xFF);
  ram_write(imm + 5, (cpu.f_reg[o.rs2 + 8] >> 40) & 0xFF);
  ram_write(imm + 6, (cpu.f_reg[o.rs2 + 8] >> 48) & 0xFF);
  ram_write(imm + 7, (cpu.f_reg[o.rs2 + 8] >> 56) & 0xFF);
}

void sv_c_j(operand_cj_t o) {
  u32 imm =   o.imm & 0b10000001110;
  imm     |= (o.imm & 0b00000000001) << 5;
  imm     |= (o.imm & 0b00000010000) << 3;
  imm     |= (o.imm & 0b00000100000) << 1;
  imm     |= (o.imm & 0b00001000000) << 3;
  imm     |=  o.imm & 0b00110000000;
  imm     |= (o.imm & 0b01000000000) >> 4;

  if (imm & 0x400) imm |= 0xFFFFFC00;
  cpu.pc += (signed)imm;
}

void sv_c_jal(operand_cj_t o) {
  u32 imm =   o.imm & 0b10000001110;
  imm     |= (o.imm & 0b00000000001) << 5;
  imm     |= (o.imm & 0b00000010000) << 3;
  imm     |= (o.imm & 0b00000100000) << 1;
  imm     |= (o.imm & 0b00001000000) << 3;
  imm     |=  o.imm & 0b00110000000;
  imm     |= (o.imm & 0b01000000000) >> 4;

  x1 = cpu.pc + 2;

  if (imm & 0x400) imm |= 0xFFFFFC00;
  cpu.pc += (signed)imm;
}

void sv_c_jr(operand_cr_t o) {
  if (!o.rs1) return;

  cpu.pc += (signed)cpu.reg[o.rs1];
}

void sv_c_jalr(operand_cr_t o) {
  if (!o.rs1) return;

  x1 = cpu.pc + 2;
  cpu.pc += (signed)cpu.reg[o.rs1];
}

void sv_c_beqz(operand_cb_t o) {
  if (cpu.reg[o.rs1 + 8]) return;

  u32 imm =   o.imm & 0b00000110;
  imm     |= (o.imm & 0b01100000) >> 2;
  imm     |= (o.imm & 0b00011000) << 2;
  imm     |= (o.imm & 0b00000001) << 5;
  imm     |= (o.imm & 0b10000000) << 1;

  if (o.imm & 0x80) imm |= 0xFFFFFF00;
  cpu.pc += (signed)imm;
}

void sv_c_bnez(operand_cb_t o) {
  if (!cpu.reg[o.rs1 + 8]) return;

  u32 imm =   o.imm & 0b00000110;
  imm     |= (o.imm & 0b01100000) >> 2;
  imm     |= (o.imm & 0b00011000) << 2;
  imm     |= (o.imm & 0b00000001) << 5;
  imm     |= (o.imm & 0b10000000) << 1;

  if (o.imm & 0x80) imm |= 0xFFFFFF00;
  cpu.pc += (signed)imm;
}

void sv_c_li(operand_ci_t o) {
  if (!o.rs1) return;

  cpu.reg[o.rs1] = (signed)o.imm;
}

void sv_c_lui(operand_ci_t o) {
  if (!o.rs1 || (o.rs1 & 2) || !o.imm) return;

  cpu.reg[o.rs1] = ((signed)o.imm) << 12;
}

void sv_c_addi(operand_ci_t o) {
  if (!o.rs1 || !o.imm) return;

  cpu.reg[o.rs1] = cpu.reg[o.rs1] + (signed)o.imm;
}

void sv_c_addi16sp(operand_ci_t o) {
  u32 imm;
  imm =   o.imm & 0b010000;
  imm |= (o.imm & 0b000001) << 5;
  imm |= (o.imm & 0b001000) << 3;
  imm |= (o.imm & 0b000110) << 6;
  imm |= (o.imm & 0b100000) << 4;

  if (imm & 0x0200) imm |= 0xFFFFFE00;

  x2 += (signed)imm;
}

void sv_c_addi4spn(operand_ciw_t o) {
  if (!o.rd) return;

  u32 imm;
  imm =  (o.imm & 0b11000000) >> 2;
  imm |= (o.imm & 0b00000010) << 1;
  imm |= (o.imm & 0b00000001) << 3;
  imm |= (o.imm & 0b00111100) << 4;

  if (imm & 0x0200) imm |= 0xFFFFFE00;

  cpu.reg[o.rd + 8] = x2 + (signed)imm;
}

void sv_c_slli(operand_ci_t o) {
  if (!o.rs1) return;

  cpu.reg[o.rs1] = cpu.reg[o.rs1] << o.imm;
}

void sv_c_srli(operand_cb_t o) {
  if (!o.rs1) return;

  int mask = ~(-1 << o.imm) << (32 - o.imm);
  cpu.reg[o.rs1] = ~mask & ((cpu.reg[o.rs1] >> o.imm) | mask);
}

void sv_c_srai(operand_cb_t o) {
  if (!o.rs1) return;

  u32 ux = (unsigned)cpu.reg[o.rs1];
  u32 signbit = ux >> 31;
  cpu.reg[o.rs1] = (ux >> o.imm) | (((0 - signbit) << 1) << (31 - o.imm));
}

void sv_c_andi(operand_cb_t o) {
  cpu.reg[o.rs1] = cpu.reg[o.rs1] & (signed)o.imm;
}

void sv_c_mv(operand_cr_t o) {
  if (!o.rs1 || !o.rs2) return;

  cpu.reg[o.rs1] = cpu.reg[o.rs2];
}

void sv_c_add(operand_cr_t o) {
  if (!o.rs1 || !o.rs2) return

  cpu.reg[o.rs1] += cpu.reg[o.rs2];
}

void sv_c_xor(operand_ca_t o) {
  cpu.reg[o.rs1 + 8] ^= cpu.reg[o.rs2 + 8];
}

void sv_c_or(operand_ca_t o) {
  cpu.reg[o.rs1 + 8] |= cpu.reg[o.rs2 + 8];
}

void sv_c_and(operand_ca_t o) {
  cpu.reg[o.rs1 + 8] &= cpu.reg[o.rs2 + 8];
}

void sv_c_sub(operand_ca_t o) {
  cpu.reg[o.rs1 + 8] -= cpu.reg[o.rs2 + 8];
}

void sv_c_nop();
void sv_c_ebreak();
void sv_illegal();