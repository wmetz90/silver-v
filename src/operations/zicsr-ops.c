#include "zicsr-ops.h"

/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   zicsr-ops.c
 *
 *   Implements functions to handle operations belonging to the Zicsr extension
 *   (Control and Status Registers) to the RV32I instruction set.
 * =============================================================================
 */



void sv_csrrw(operand_r_t o);

void sv_csrrs(operand_r_t o);

void sv_csrrc(operand_r_t o);

void sv_csrrwi(operand_r_t o);

void sv_csrrsi(operand_r_t o);

void sv_csrrci(operand_r_t o);