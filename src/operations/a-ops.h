#ifndef __silver_v_a_ops_h
#define __silver_v_a_ops_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"
#include "operands.h"



/*
 * =============================================================================
 *   a-ops.h
 *
 *   Prototypes functions to handle operations belonging to the A extension
 *   (atomic memory operations) to the RV32I instruction set.
 * =============================================================================
 */



void sv_slr(operand_r_t o);
void sv_ssc(operand_r_t o);
void sv_amoswap(operand_r_t o);
void sv_amoadd(operand_r_t o);
void sv_amoxor(operand_r_t o);
void sv_amoand(operand_r_t o);
void sv_amoor(operand_r_t o);
void sv_amomin(operand_r_t o);
void sv_amomax(operand_r_t o);
void sv_amominu(operand_r_t o);
void sv_amomaxu(operand_r_t o);



#endif