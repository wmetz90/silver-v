#ifndef __silver_v_i_ops_h
#define __silver_v_i_ops_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"



/*
 * =============================================================================
 *   i-ops.h
 *
 *   Prototypes functions to handle operations belonging to the RV32I base
 *   instruction set.
 * =============================================================================
 */



void sv_lui(operand_u_t o);
void sv_auipc(operand_u_t o);
void sv_jal(operand_j_t o);
void sv_jalr(operand_i_t o);
void sv_beq(operand_b_t o);
void sv_bne(operand_b_t o);
void sv_blt(operand_b_t o);
void sv_bge(operand_b_t o);
void sv_bltu(operand_b_t o);
void sv_bgeu(operand_b_t o);
void sv_lb(operand_i_t o);
void sv_lh(operand_i_t o);
void sv_lw(operand_i_t o);
void sv_lbu(operand_i_t o);
void sv_lhu(operand_i_t o);
void sv_sb(operand_s_t o);
void sv_sh(operand_s_t o);
void sv_sw(operand_s_t o);
void sv_addi(operand_i_t o);
void sv_nop();
void sv_slti(operand_i_t o);
void sv_sltiu(operand_i_t o);
void sv_xori(operand_i_t o);
void sv_ori(operand_i_t o);
void sv_andi(operand_i_t o);
void sv_slli(operand_i_t o);
void sv_srli(operand_i_t o);
void sv_srai(operand_i_t o);
void sv_add(operand_r_t o);
void sv_sub(operand_r_t o);
void sv_sll(operand_r_t o);
void sv_slt(operand_r_t o);
void sv_sltu(operand_r_t o);
void sv_xor(operand_r_t o);
void sv_srl(operand_r_t o);
void sv_sra(operand_r_t o);
void sv_or(operand_r_t o);
void sv_and(operand_r_t o);
void sv_fence(operand_i_t o);
void sv_ecall();
void sv_ebreak();



#endif