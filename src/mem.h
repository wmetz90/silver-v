#ifndef __silver_v_mem_h
#define __silver_v_mem_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"



/*
 * =============================================================================
 *   mem.h
 *
 *   Definitions and prototypes related to memory access.
 * =============================================================================
 */



#define KB 1024
#define MB 1048576



extern u8 *   ram;
extern u32    ramsize = 256 * KB;    // 256KB



void ram_init();

u8   ram_read_byte (u32 addr);
void ram_write_byte(u32 addr, u8 data);

u16  ram_read_half (u32 addr);
void ram_write_half(u32 addr, u16 data);

u32  ram_read_word (u32 addr);
void ram_write_word(u32 addr, u32 data);



#endif