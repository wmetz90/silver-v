#ifndef __silver_v_opcodes_h
#define __silver_v_opcodes_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



typedef enum {
  LOAD      = 0b0000011,
  STORE     = 0b0100011,
  MADD      = 0b1000011,
  BRANCH    = 0b1100011,
  LOAD_FP   = 0b0000111,
  STORE_FP  = 0b0100111,
  MSUB      = 0b1000111,
  JALR      = 0b1100111,
  // custom-0
  // custom-1
  NMSUB     = 0b1001011,
  // reserved
  MISC_MEM  = 0b0001111,
  AMO       = 0b0101111,
  NMADD     = 0b1001111,
  JAL       = 0b1101111,
  OP_IMM    = 0b0010011,
  OP        = 0b0110011,
  OP_FP     = 0b1010011,
  SYSTEM    = 0b1110011,
  AUIPC     = 0b0010111,
  LUI       = 0b0110111,
  // reserved
  // reserved
  OP_IMM_32 = 0b0011011,
  OP_32     = 0b0111011,
  // custom-2/rv128
  // custom-3/rv128

  // Compressed major opcodes
  C0 = 0b00,
  C1 = 0b01,
  C2 = 0b10,
} major_opcodes;



#endif