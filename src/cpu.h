#ifndef __silver_v_cpu_h
#define __silver_v_cpu_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   cpu.h
 *
 *   Defines the rv_cpu_t struct, which models the CPU, and prototypes
 *   functions associated with it.
 * =============================================================================
 */



#include "registers.h"
#include "types.h"



// The alignment boundary and maximum instruction length.
#define IALIGN 16
#define ILEN 32
#define FLEN 64



// =======
//   CPU
// =======
typedef struct {
  // RV32I registers, all 32 bits wide.

  // Registers x0-x31. x0 is always wired to 0x00000000.
  u32 reg[32];

  // The program counter.
  u32 pc;



  // Floating-point extension registers, 64 bits wide because we have both the
  // F and D extensions. Single precision values will be NaN-boxed. Declared as
  // `u64` instead of double for easier manipulation and easier storage of
  // single-precision values.
  u64 f_reg[32];

  // The Floating-point Control and Status Register. Holds informational flags.
  u32 fcsr;

} rv_cpu;



extern rv_cpu cpu;



void cpu_init();



#endif