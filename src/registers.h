#ifndef __silver_v_registers_h
#define __silver_v_registers_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



/*
 * =============================================================================
 *   registers.h
 *
 *   Defines alternate names for the CPU's registers for ease of use. This
 *   file has no function or type definitions.
 * =============================================================================
 */



/*
 * =================
 *   GENERIC NAMES
 * =================
 */

/*
 * RV32I Registers
 */

#define x0  cpu.reg[0]
#define x1  cpu.reg[1]
#define x2  cpu.reg[2]
#define x3  cpu.reg[3]
#define x4  cpu.reg[4]
#define x5  cpu.reg[5]
#define x6  cpu.reg[6]
#define x7  cpu.reg[7]
#define x8  cpu.reg[8]
#define x9  cpu.reg[9]
#define x10 cpu.reg[10]
#define x11 cpu.reg[11]
#define x12 cpu.reg[12]
#define x13 cpu.reg[13]
#define x14 cpu.reg[14]
#define x15 cpu.reg[15]
#define x16 cpu.reg[16]
#define x17 cpu.reg[17]
#define x18 cpu.reg[18]
#define x19 cpu.reg[19]
#define x20 cpu.reg[20]
#define x21 cpu.reg[21]
#define x22 cpu.reg[22]
#define x23 cpu.reg[23]
#define x24 cpu.reg[24]
#define x25 cpu.reg[25]
#define x26 cpu.reg[26]
#define x27 cpu.reg[27]
#define x28 cpu.reg[28]
#define x29 cpu.reg[29]
#define x30 cpu.reg[30]
#define x31 cpu.reg[31]

/*
 * F/D Extension Registers
 */

#define f0  cpu.f_reg[0]
#define f1  cpu.f_reg[1]
#define f2  cpu.f_reg[2]
#define f3  cpu.f_reg[3]
#define f4  cpu.f_reg[4]
#define f5  cpu.f_reg[5]
#define f6  cpu.f_reg[6]
#define f7  cpu.f_reg[7]
#define f8  cpu.f_reg[8]
#define f9  cpu.f_reg[9]
#define f10 cpu.f_reg[10]
#define f11 cpu.f_reg[11]
#define f12 cpu.f_reg[12]
#define f13 cpu.f_reg[13]
#define f14 cpu.f_reg[14]
#define f15 cpu.f_reg[15]
#define f16 cpu.f_reg[16]
#define f17 cpu.f_reg[17]
#define f18 cpu.f_reg[18]
#define f19 cpu.f_reg[19]
#define f20 cpu.f_reg[20]
#define f21 cpu.f_reg[21]
#define f22 cpu.f_reg[22]
#define f23 cpu.f_reg[23]
#define f24 cpu.f_reg[24]
#define f25 cpu.f_reg[25]
#define f26 cpu.f_reg[26]
#define f27 cpu.f_reg[27]
#define f28 cpu.f_reg[28]
#define f29 cpu.f_reg[29]
#define f30 cpu.f_reg[30]
#define f31 cpu.f_reg[31]


/*
 * ======================
 *   ABI REGISTER NAMES
 * ======================
 */


/*
 * RV32I Registers
 */

// x0 is hardwired to 0x00000000
#define zero cpu.reg[0]

// Return address for jumps
#define ra cpu.reg[1]

// Stack pointer
#define sp cpu.reg[2]

// Global pointer
#define gp cpu.reg[3]

// Thread pointer
#define tp cpu.reg[4]

// Temporary register 0 / Alternate return address
#define t0 cpu.reg[5]

// Temporary registers 1-2
#define t1 cpu.reg[6]
#define t2 cpu.reg[7]

// Saved register 0 and frame pointer both use x8
#define s0 cpu.reg[8]
#define fp cpu.reg[8]

// Saved register 1
#define s1 cpu.reg[9]

// Return value or function argument 0
#define a0 cpu.reg[10]

// Return value or function argument 1
#define a1 cpu.reg[11]

// Function arguments 2-7
#define a2 cpu.reg[12]
#define a3 cpu.reg[13]
#define a4 cpu.reg[14]
#define a5 cpu.reg[15]
#define a6 cpu.reg[16]
#define a7 cpu.reg[17]

// Saved registers 2-11
#define s2  cpu.reg[18]
#define s3  cpu.reg[19]
#define s4  cpu.reg[20]
#define s5  cpu.reg[21]
#define s6  cpu.reg[22]
#define s7  cpu.reg[23]
#define s8  cpu.reg[24]
#define s9  cpu.reg[25]
#define s10 cpu.reg[26]
#define s11 cpu.reg[27]

// Temporary registers 3-6
#define t3 cpu.reg[28]
#define t4 cpu.reg[29]
#define t5 cpu.reg[30]
#define t6 cpu.reg[31]


/*
 * F/D Extension Registers
 */

// Temporary registers 0-7
#define ft0 cpu.f_reg[0]
#define ft1 cpu.f_reg[1]
#define ft2 cpu.f_reg[2]
#define ft3 cpu.f_reg[3]
#define ft4 cpu.f_reg[4]
#define ft5 cpu.f_reg[5]
#define ft6 cpu.f_reg[6]
#define ft7 cpu.f_reg[7]
// Saved registers 0-1
#define fs0 cpu.f_reg[8]
#define fs1 cpu.f_reg[9]
// Return value or function argument 0
#define fa0 cpu.f_reg[10]
// Return value or function argument 1
#define fa1 cpu.f_reg[11]
// Function arguments 2-7
#define fa2 cpu.f_reg[12]
#define fa3 cpu.f_reg[13]
#define fa4 cpu.f_reg[14]
#define fa5 cpu.f_reg[15]
#define fa6 cpu.f_reg[16]
#define fa7 cpu.f_reg[17]
// Saved registers 2-11
#define fs2  cpu.f_reg[18]
#define fs3  cpu.f_reg[19]
#define fs4  cpu.f_reg[20]
#define fs5  cpu.f_reg[21]
#define fs6  cpu.f_reg[22]
#define fs7  cpu.f_reg[23]
#define fs8  cpu.f_reg[24]
#define fs9  cpu.f_reg[25]
#define fs10 cpu.f_reg[26]
#define fs11 cpu.f_reg[27]
// Temporary registers 8-11
#define ft8  cpu.f_reg[28]
#define ft9  cpu.f_reg[29]
#define ft10 cpu.f_reg[30]
#define ft11 cpu.f_reg[31]



#endif