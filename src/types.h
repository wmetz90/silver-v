#ifndef __silver_v_types_h
#define __silver_v_types_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */


/*
 * =============================================================================
 *   types.h
 *
 *   Defines type aliases to use throughout the program.
 * =============================================================================
 */



// Host type checks
static_assert(sizeof(short)     == 2, "short must be 16 bits.");
static_assert(sizeof(int)       == 4, "int must be 32 bits.");
static_assert(sizeof(long long) == 8, "long long must be 64 bits.");
static_assert(sizeof(float)     == 4, "float must be 32 bits.");
static_assert(sizeof(double)    == 8, "double must be 64 bits.");

// Type aliases
typedef  signed char         s8;
typedef  unsigned char       u8;
typedef  signed short int    s16;
typedef  unsigned short int  u16;
typedef  signed int          s32;
typedef  unsigned int        u32;
typedef  signed long long    s64;
typedef  unsigned long long  u64;
typedef  float               f32;
typedef  double              f64;

// Project typedefs

// Instructions are read as 16 bit parcels, since not all instructions are the
// full 32 bit width.
typedef u16 parcel_t;

// A full 32 bit instruction.
typedef u32 instruction_t;
// A 16 bit compact instruction.
typedef u16 compactinstruction_t;



#endif