#ifndef __silver_v_operands_h
#define __silver_v_operands_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"



/*
 * =============================================================================
 *   operands.h
 *
 *   Defines functions and structures used to pull parameters from
 *   instructions.
 * =============================================================================
 */



// ================================
//   STANDARD INSTRUCTION FORMATS
// ================================



// Type R includes a destination register and two parameter registers.
typedef struct {
  unsigned rd  : 5;
  unsigned rs1 : 5;
  unsigned rs2 : 5;
} operand_r_t;

// Type I includes a destination register, a parameter register, and a 12 bit
// immediate value.
typedef struct {
  unsigned rd  : 5;
  unsigned rs1 : 5;
  unsigned imm : 12;
} operand_i_t;

// Type S includes two parameter registers and a 12 bit immediate value.
typedef struct {
  unsigned rs1 : 5;
  unsigned rs2 : 5;
  unsigned imm : 12;
} operand_s_t;

// Type B includes two parameter registers and a 12 bit immediate value. For
// information on the difference between this and type S, check section 2.3 of
// the official specification.
typedef struct {
  unsigned rs1 : 5;
  unsigned rs2 : 5;
  unsigned imm : 12;
} operand_b_t;

// Type U includes a destination register and a 20 bit immediate value.
typedef struct {
  unsigned rd  : 5;
  unsigned imm : 20;
} operand_u_t;

// Type J includes a destination register and a 20 bit immediate value. For
// information on the difference between this and type U, check section 2.3 of
// the official specification.
typedef struct {
  unsigned rd  : 5;
  unsigned imm : 20;
} operand_j_t;

// Type R4 includes 3 source registers and a destination register. This type is
// primarily used for floating point fused multiply-add operations.
typedef struct {
  unsigned rd  : 5;
  unsigned rs1 : 5;
  unsigned rs2 : 5;
  unsigned rs3 : 5;
} operand_r4_t;


operand_r_t get_r_operands(u32 op) {
  operand_r_t result;

  result.rd  = (op >>  7) & 0x1F;
  result.rs1 = (op >> 15) & 0x1F;
  result.rs2 = (op >> 20) & 0x1F;

  return result;
}

operand_i_t get_i_operands(u32 op) {
  operand_i_t result;

  result.rd  = (op >>  7) & 0x1F;
  result.rs1 = (op >> 15) & 0x1F;
  result.imm = (op >> 20) & 0xFFF;

  return result;
}

operand_s_t get_s_operands(u32 op) {
  operand_s_t result;

  result.rs1 =  (op >> 15) & 0x1F;
  result.rs2 =  (op >> 20) & 0x1F;
  result.imm =  (op >>  7) & 0x1F;
  result.imm |= (op >> 20) & 0xFE0;

  return result;
}

operand_b_t get_b_operands(u32 op) {
  operand_b_t result;

  result.rs1 =  (op >> 15) & 0x1F;
  result.rs2 =  (op >> 20) & 0x1F;
  result.imm =  (op >>  8) & 0xF;
  result.imm |= (op >> 20) & 0x1F0;
  result.imm |= (op <<  3) & 0x400;
  result.imm |= (op >> 20) & 0x800;

  return result;
}

operand_u_t get_u_operands(u32 op) {
  operand_u_t result;

  result.rd  = (op >> 7) & 0x1F;
  result.imm = (op >> 12) & 0xFFFFF;

  return result;
}

operand_j_t get_j_operands(u32 op) {
  operand_j_t result;

  result.rd  =  (op >>  7) & 0x1F;
  result.imm =  (op >> 21) & 0x3FF;
  result.imm |= (op >>  9) & 0x400;
  result.imm |= op & 0xF800;
  result.imm |= (op >> 5) & 0x80000;
  return result;
}

operand_r4_t get_r4_operands(u32 op) {
  operand_r4_t result;

  result.rd  = (op >> 7)  & 0x1F;
  result.rs1 = (op >> 15) & 0x1F;
  result.rs2 = (op >> 20) & 0x1F;
  result.rs3 = (op >> 27) & 0x1F;

  return result;
}



// ==================================
//   COMPRESSED INSTRUCTION FORMATS
// ==================================

// Type CR is a compressed instruction format that holds either two operand
// registers, or a destination register and a operand register. rs1 can serve as
// both operand and destination for this purpose.
typedef struct {
  unsigned rs1 : 5; // rs1 can also be referred to as rd.
  unsigned rs2 : 5;
} operand_cr_t;

// Type CI is a compressed instruction format that holds an operand register
// and a 6 bit immediate value. rs1 can serve as both operand and destination
// for this purpose.
typedef struct {
  unsigned rs1 : 5; // rs1 can also be referred to as rd.
  unsigned imm : 6;
} operand_ci_t;

// Type CSS is a compressed instruction format that holds an operand register
// and a 6 bit immediate value. For information on how this differs from type
// CI, check chapter 16.2 of the official specification.
typedef struct {
  unsigned rs2 : 5;
  unsigned imm : 6;
} operand_css_t;

// Type CIW is a compressed instruction format that holds a 3 bit destination
// register and an 8 bit immediate value. Which registers can be used depends
// on the instruction, see the official specification for more detail.
typedef struct {
  unsigned rd  : 3;
  unsigned imm : 8;
} operand_ciw_t;

// Type CL is a compressed instruction format that holds a 3 bit destination
// register, a 3 bit operand register, and a 5 bit immediate value. Which
// registers can be used depends  on the instruction, see the official
// specification for more detail. It is typically used for load instructions.
typedef struct {
  unsigned rs1  : 3;
  unsigned rs2 : 3;
  unsigned imm : 5;
} operand_cl_t;

// Type CS is a compressed instruction format that holds a 3 bit destination
// register, a 3 bit operandd register, and a 5 bit immediate value. For
// information on how this differs from type CL, see the official
// specification. It is typically used for store instructions.
typedef struct {
  unsigned rs1  : 3;
  unsigned rs2 : 3;
  unsigned imm : 5;
} operand_cs_t;

// Type CA is a compressed instruction format that holds either two 3 bit
// operand registers, or a 3 bit operand register and a 3 bit destination
// register. rs1 can be referred to as rd for this purpose. It is typically
// used for arithmetic instructions.
typedef struct {
  unsigned rs1 : 3; // Can also be referred to as rd.
  unsigned rs2 : 3;
} operand_ca_t;

// Type CB is a compressed instruction format that holds a 3 bit operand
// register and an 8 bit immediate offset value. It is typically used for
// branch instructions.
typedef struct {
  unsigned rs1 : 3;
  unsigned imm : 8;
} operand_cb_t;

// Type CJ is a compressed instruction format that holds an 11 bit immediate
// value, the jump target. It is used for jump instructions.
typedef struct {
  unsigned imm : 11;
} operand_cj_t;



operand_cr_t get_cr_operands(u16 op) {
  operand_cr_t result;

  result.rs1 = (op >> 7) & 0x1F;
  result.rs2 = (op >> 2) & 0x1F;

  return result;
}

operand_ci_t get_ci_operands(u16 op) {
  operand_ci_t result;

  result.rs1 = (op >> 7) & 0x1F;

  result.imm =  (op >> 2) & 0x1F;
  result.imm |= (op >> 7) & 0x20;

  return result;
}

operand_css_t get_css_operands(u16 op) {
  operand_css_t result;

  result.rs2 = (op >> 2) & 0x1F;
  result.imm = (op >> 7) & 0x3F;

  return result;
}

operand_ciw_t get_ciw_operands(u16 op) {
  operand_ciw_t result;

  result.rd  = (op >> 2) & 0x7;
  result.imm = (op >> 5) & 0xFF;

  return result;
}

operand_cl_t get_cl_operands(u16 op) {
  operand_cl_t result;

  result.rs2  =  (op >> 2) & 0x7;
  result.rs1 =  (op >> 7) & 0x7;
  result.imm =  (op >> 5) & 0x3;
  result.imm |= (op >> 8) & 0x1C;

  return result;
}

operand_cs_t get_cs_operands(u16 op) {
  operand_cs_t result;

  result.rs2 =  (op >> 2) & 0x7;
  result.rs1 =  (op >> 7) & 0x7;
  result.imm =  (op >> 5) & 0x3;
  result.imm |= (op >> 8) & 0x1C;

  return result;
}

operand_ca_t get_ca_operands(u16 op) {
  operand_ca_t result;

  result.rs2 = (op >> 2) & 0x7;
  result.rs1 = (op >> 7) & 0x7;

  return result;
}

operand_cb_t get_cb_operands(u16 op) {
  operand_cb_t result;

  result.rs1 =  (op >> 7) & 0x7;
  result.imm =  (op >> 2) & 0x1F;
  result.imm |= (op >> 7) & 0x20;

  return result;
}

operand_cj_t get_cj_operands(u16 op) {
  operand_cj_t result;

  result.imm = (op >> 2) & 0x7FF;

  return result;
}



#endif