#include "mem.h"



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include <byteswap.h>



/*
 * =============================================================================
 *   mem.c
 *
 *   Implements functions defined in mem.h.
 * =============================================================================
 */



void ram_init() {
  ram = malloc(ramsize);
}


// Separate read and write functions are used for future implementation of
// memory mapped devices and for easier implementation of instructions.


u8 ram_read_byte(u32 addr) {
  // If we're out of the range of memory, just return zero.
  if (addr >= ramsize) return 0;

  // Get the byte from that location in memory.
  return ram[addr];
}

void ram_write_byte(u32 addr, u8 data) {
  // If we're out of the range of memory, just return.
  if (addr >= ramsize) return;

  // Set that location in memory to the data.
  ram[addr] = data;
}



u16  ram_read_half (u32 addr) {
  // If we're out of the range of memory, just return zero.
  if (addr >= ramsize) return 0;

  // Instantiate the return value.
  u16 result;

  // If we can't get a full half-word, then get the first byte.
  if (addr == ramsize - 1) return (u16)ram[addr];

  // Get the data from memory and arrange it in the correct order.
  result =  ram[addr];
  result |= ram[addr + 1] << 8;

  return result;
}

void ram_write_half(u32 addr, u16 data) {
  // If we're out of the range of memory, just return.
  if (addr >= ramsize) return;

  // If we can't write a full half-word, then write the first byte.
  if (addr == ramsize - 1) ram[addr] = (u8)(data & 0xFF);

  // Write the data in the correct order.
  ram[addr] = (u8)(data & 0xFF);
  ram[addr + 1] = (u8)(data >> 8);

  return;
}



u32  ram_read_word (u32 addr) {
  // If we're out of the range of memory, just return zero.
  if (addr >= ramsize) return 0;

  // Instantiate the return value.
  u32 result;

  // If we can't read a full word, then just read as much as we can.
  if (addr >= ramsize - 3) {
    // memcpy is easier than figuring out how many bytes to grab and grabbing
    // each individually.
    memcpy(&result, ram + addr, ramsize - addr);
  } else {
    // Read a word.
    memcpy(&result, ram + addr, 4);
  }

  // Transform from little to big endian.
  result = __bswap_32(result);

  return result;
}

void ram_write_word(u32 addr, u32 data) {
  // If we're out of the range of memory, just return.
  if (addr >= ramsize) return;

  // Transform from big to little endian.
  data = __bswap_32(data);

  // If we can't write a full word, then just write as much as we can.
  if (addr >= ramsize - 3) {
    memcpy(ram + addr, &data, ramsize - addr);
  } else {
    memcpy(ram + addr, &data, 4);
  }
}