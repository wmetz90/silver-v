#ifndef __silver_v_csr_h
#define __silver_v_csr_h



/*
 * =============================================================================
 *   Silver-V
 *   Copyright (C) 2019  Will Metz
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * =============================================================================
 */



#include "types.h"



/*
 * =============================================================================
 *   csr.h
 *
 *   Prototypes and definitions related to CPU CSRs (Control and Status
 *   Registers). Since we're only emulating a single hart at the moment, we
 *   only need to keep track of the base 4096.
 * =============================================================================
 */



extern u32 * csr;



void csr_init();



#endif